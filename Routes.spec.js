//created by: Mayura Vyas Date: 05/07/2019//
//This is a automated script for login to R4 application//
//Select site picker and then create Routes// 

import { equal } from "assert";

describe('Testing R4 Routes creation',  () => {

        it('Visit to insite site',  () => {
        cy.visit('https://redesign-web.sbminsite.com/login')
        
        
                        
    })
        it('Login to application', () => {
        //Login to application with username and pwd//
        cy.fixture("credentials").as("login") //reading cred from file with alias login
        cy.get("@login").then((login) => {
            cy.get('[data-i="username"] > input').type(login.UserName)
            cy.get('[data-i="password"] > input').type(login.Password)
            
        })
        
        cy.get('button').click()
    })
         // This is for Site picker logic with client and site and program// 
         it("Site picker logic", () => {
            
                   // cy.get('.intro-location-text').click({force:true})// this is for site picker
                     
                      //  cy.get('.cm-dropdown > .dropdown').click().parent().contains('li', 'CBRE').click()
                                          
                      //  cy.get(':nth-child(3) > .cm-dropdown > .selection-trigger > .fa').click().parentsUntil().contains('li', 'Puget Sound').click()
                      //  cy.get(':nth-child(4) > .cm-dropdown > .selection-trigger').click().parentsUntil().contains('li', 'Custodial').click()
                      //  cy.get('.cm-picker-context > .actions > .primary').click()
                        //cy.wait(10000)
        //Click on left Navigation -Governance link//
      
     
                                             cy.get('.nav-hamburger').click()
                                                cy.get('.nav-level-title').contains('Governance').click()
                                                    cy.get('.nav-level-title').contains('Route Builder').click() //Route builder page
                                                    cy.pause()
                                                 
                              
                      
                           
        //Create Route---step1//
        // This is for position//
         cy.get("select[name='positionId']").select('Recycle Tech').should('have.value','4')
         cy.get("select[name='positionId']").select('Custodian').should('have.value','1')
         cy.get("select[name='positionId']").select('Sustainability Manager').should('have.value','8')
         cy.get("select[name='positionId']").select('Recycle Lead').should('have.value','7')
            //This is for program//
            cy.get('#multi-select-trigger').click()
            cy.get('.items-list > :nth-child(1)').click()
            //This is for Resposinble Associate//
            cy.get("select[name='associateId']").select('43').should('have.value','43')
            // This is for Responsible Manager//
            cy.get("select[name='managerId']").select('55').should('have.value','55')
            cy.get('input[name="routeName"]').type('This is a test')
            //This is for shift//
            cy.get("select[name='shiftId']").select('Day').should('have.value', '16') //15-night, 72 swing, 231 on call
              
                              cy.get('.row-layout-container > .primary').click() //Next button
                  
       // This is for next step2//
       cy.get("input[name='startTime']").type('09:00')    //starttime

       cy.get("input[name='endTime']").type('18:00') //end time

       cy.get("select[name='startLocationId']").select('102').should('have.value','102') //building 1 location start
       cy.get("input[name='startDuration']").type('120')
       cy.get("select[name='endLocationId']").select('102').should('have.value','102') //building 1 location end 
       cy.get("input[name='endDuration']").type('220')
       
       cy.get('textarea').first().type('Test') //instructions
       cy.get('textarea').type('This is a second test')
       cy.get('.row-layout-container > .primary').click() //sav n continue


               

                              
       
                
    })
})
       


        
                  
         