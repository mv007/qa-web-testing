//created by: Mayura Vyas Date: 05/07/2019//
//This is a automated script for login to R4 application//
//Select site picker and then create Audit// 

import { equal } from "assert";

describe('Testing Personnel-Conduct',  () => {

        it('Visit to insite site',  () => {
            cy.visit('https://redesign-web.sbminsite.com/login')
               
                        
    })
        it('Login to application', () => {
        //Login to application with username and pwd//
        cy.fixture("credentials").as("login") //reading cred from file with alias login
        cy.get("@login").then((login) => {
            cy.get('[data-i="username"] > input').type(login.UserName)
            cy.get('[data-i="password"] > input').type(login.Password)
            
        })
        
        cy.get('button').click()
    })
         // This is for Site picker logic with client and site and program// 
         it("Site picker logic", () => {
            
                    cy.get('.intro-location-text').click({force:true})// this is for site picker
                      
                        cy.get('.cm-dropdown > .dropdown').click().parent().contains('li', 'Zoetis').click() //client
                                          
                        cy.get(':nth-child(3) > .cm-dropdown > .selection-trigger > .fa').click().parentsUntil().contains('li', 'Lincoln').click()//site
                        cy.get(':nth-child(4) > .cm-dropdown > .selection-trigger').click().parentsUntil().contains('li', 'Custodial').click() //program
                        cy.get('.cm-picker-context > .actions > .primary').click() //save button
                        cy.wait(10000)
        //Click on left Navigation -Charts and details- Conduct//
           
                                             cy.get('.nav-hamburger').click() //left nav
                                                cy.get('.nav-level-title').contains('Charts & Details').click() //chart n details
                                                    cy.get('.nav-level-title').contains('Personnel').click() //personnel
                                                        cy.get('.nav-level-title').contains('Conduct').click() //conduct
                                                        
                                                        //go back to this page//
                                                     
                                                            cy.get('button').contains('New Conduct').click() // Click on New Conduct
                                                                cy.get('.employee-selector-wrapper > :nth-child(2) > .form-button').click()// select employee John Mann
                                                                                      
                            
                                 //Create Conduct
                                          //first conduct                            
                                    cy.get('#ConductTypes').select('Verbal Warning')
                                    cy.get('textarea').type('This is for testing')
                                    cy.get('input.form-button').click()
                                  //second conduct
                                  cy.get('button').contains('New Conduct').click() // Click on New Conduct
                                  cy.get('.employee-selector-wrapper > :nth-child(2) > .form-button').click()// select employee John Mann
                                    cy.get('#ConductTypes').select('Written Warning')
                                    cy.get('textarea').type('This is for testing')
                                    cy.get('input.form-button').click()
                                    // Third conduct
                                    cy.get('button').contains('New Conduct').click() // Click on New Conduct
                                    cy.get('.employee-selector-wrapper > :nth-child(2) > .form-button').click()// select employee John Mann
                                    cy.get('#ConductTypes').select('Final Warning')
                                    cy.get('textarea').type('This is for testing')
                                    cy.get('input.form-button').click()

                     
                                                            
                                  

                                 

         })
        
        
       })



                  
       
               

                              
       
                
 



       


        
                  
         