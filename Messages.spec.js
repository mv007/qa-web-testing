//created by: Mayura Vyas Date: 05/07/2019//
//This is a automated script for login to R4 application//
//Select site picker and then Send Survey// 

import { equal } from "assert";

describe('This is automation test suit for R4',  () => {

        it('Visit to insite site',  () => {
       cy.visit('https://bay1.sbminsite.com/login')
                                  
    })
        it("Login to application", () => {
        //Login to application with username and pwd//
        cy.fixture("credentials").as("login") //reading cred from file with alias login
        cy.get("@login").then((login) => {
            cy.get('[data-i="username"] > input').type(login.UserName)
            cy.get('[data-i="password"] > input').type(login.Password)
            
        })

        cy.get('button').click()
    })               

        it("Site picker logic", () => {
            //no need os this logic in bay1 now //
            cy.get('.intro-location-text').click({force:true})// this is for site picker
            cy.get('.cm-dropdown > .dropdown').click().parent().contains('li', 'Zoetis').click()
                                          
            //cy.get(':nth-child(3) > .cm-dropdown > .selection-trigger > .fa').click().parentsUntil().contains('li', 'Charles City').click()
           // cy.get(':nth-child(4) > .cm-dropdown > .selection-trigger').click().parentsUntil().contains('li', 'Custodial').click()
            //cy.get('.cm-picker-context > .actions > .primary').click()
            //cy.wait(10000)
        
                                           
                      
                              //Click on left Navigation -Charts and details- Send Surveys//
           
                                cy.get('.nav-hamburger').click()
                                    cy.get('.nav-level-title').contains('Charts & Details').click()
                                        cy.get('.nav-level-title').contains('Messages').click()
                                        cy.get('button').contains('Send Message').click()
                                        cy.get('[for="send to an employee"]').click()
                                        cy.get('.employee-selector-wrapper > :nth-child(1) > .form-button').click()// select employee
                                        cy.get('button').contains('Continue').click()
                                        cy.get(':nth-child(4) > .formik-input').type('This is a test message') //Title
                                        cy.get('.ck-editor__main > .ck').type('This is a test message to check in bay1') //Message header
                                        cy.get('.submit').click()



                                                              
                                                 
        })
       

                      
    })