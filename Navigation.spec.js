//created by: Mayura Vyas Date: 05/07/2019//
//This is a automated script for login to R4 application//
//Select site picker and then create Audit// 

import { equal } from "assert";

describe('Tests for Left hand Navigation',  () => {

    it('Visit to insite site',  () => {
        cy.visit('https://bay1.sbminsite.com/login')
        
        
                        
    })
    it('Login to application', () => {
        //Login to application with username and pwd//
        cy.fixture("credentials").as("login") //reading cred from file with alias login
        cy.get("@login").then((login) => {
            cy.get('[data-i="username"] > input').type(login.UserName)
            cy.get('[data-i="password"] > input').type(login.Password)
            
        })
        
        cy.get('button').click()
        cy.get('.nav-hamburger').click()


        cy.get('.nav-level-title').contains('Charts & Details').click()
        cy.get('.nav-level-title').contains('Quality').click()
        cy.get('.nav-level-button ').contains('Audits').click()

        cy.pause() //
        //This is for bottom page//
        cy.get('.nav-menu-top').children().click({multiple:true})
        cy.get('.nav-menu-bottom').children().click({multiple:true})
       
        
    })
})
