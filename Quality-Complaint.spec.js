//created by: Mayura Vyas Date: 05/07/2019//
//This is a automated script for login to R4 application//
//Select site picker and then create Complaint// 

import { equal } from "assert";

describe('Testing for Complaint',  () => {

    it('Visit to insite site',  () => {
        cy.visit('https://redesign-web.sbminsite.com/login')
                                  
    })
    it('Login to application', () => {
        //Login to application with username and pwd//
        cy.fixture("credentials").as("login") //reading cred from file with alias login
        cy.get("@login").then((login) => {
            cy.get('[data-i="username"] > input').type(login.UserName)
            cy.get('[data-i="password"] > input').type(login.Password)
            
        })
       
        cy.get('button').click()
    })
              
        // This is for Site picker logic with client and site and program//       
        it("Site picker logic", () => {  
                cy.get('.intro-location-text').click({force:true})// this is for site picker
                     
                         cy.get('.cm-dropdown > .dropdown').click().parent().contains('li', 'Zoetis').click()
                                          
                           cy.get(':nth-child(3) > .cm-dropdown > .selection-trigger > .fa').click().parentsUntil().contains('li', 'Lincoln').click()
                                 cy.get(':nth-child(4) > .cm-dropdown > .selection-trigger').click().parentsUntil().contains('li', 'Custodial').click()
                                   cy.get('.cm-picker-context > .actions > .primary').click()
        // Now Click on view additional details tab for Audits//
                    cy.wait(20000)
                    cy.get('.nav-hamburger').click() //click on left hand nav
                                                cy.get('.nav-level-title').contains('Charts & Details').click()
                                                  cy.get('.nav-level-title').contains('Quality').click()
                                                          cy.get('.nav-level-button').contains('Complaints').click({force:true})
                                                                cy.get('button').contains('New Complaint').click()

       // cy.get('[data-active="false"] > span').click() //click on details tab from home dashboard
       // cy.get(':nth-child(1) > .cm-slider > :nth-child(2) > [style="transform: translateX(0px);"] > .content > .tiles > :nth-child(1) > .tile-title').click() //for complaint
        //cy.get('.large').click() //create compliant
        
        cy.get('.dropdown-trigger').click().parent().contains('li','Custodial').click() 
        cy.get('.form-button').click()

        cy.get('form').within(() => {
            //cy.get('textarea').contains('Description').type('This is a test')//description
            cy.get('textarea[name=Description]').type('This is a test') //description
            cy.get('input[name=CustomerName]').type('Mayura') //cust name
            
        cy.get(':nth-child(3) > [style="width: 50%; margin-left: 4px;"] > .formik-group > .dropdown-container > .dropdown-trigger').click().parent().contains('li','True Complaint').click()
        cy.get(':nth-child(4) > [style="width: 50%; margin-left: 4px;"] > .formik-group > .dropdown-container > .dropdown-trigger').click().parent().contains('li','Dusting').click()
        cy.get(':nth-child(5) > [style="width: 50%; margin-right: 4px;"] > .formik-group > .dropdown-container > .dropdown-trigger').click().parent().contains('li','Farm').click()
        cy.get(':nth-child(5) > [style="width: 50%; margin-left: 4px;"] > .formik-group > .dropdown-container > .dropdown-trigger').click().parent().contains('li','1').click()
        cy.get(':nth-child(6) > [style="width: 50%; margin-right: 4px;"] > .formik-group > .dropdown-container > .dropdown-trigger').click().parent().contains('li','No').click()
  


            cy.get('textarea[name=Note]').type('Testing R4 components') //notes
            

        })
        
cy.get('form').submit()
      

    })
})