//created by: Mayura Vyas Date: 05/07/2019//
//This is a automated script for login to R4 application//
//Select site picker and then create Audit// 

import { equal } from "assert";

describe('Testing R4 Audits',  () => {

        it('Visit to insite site',  () => {
        cy.visit('https://redesign-web.sbminsite.com/login')
        
        
                        
    })
        it('Login to application', () => {
        //Login to application with username and pwd//
        cy.fixture("credentials").as("login") //reading cred from file with alias login
        cy.get("@login").then((login) => {
            cy.get('[data-i="username"] > input').type(login.UserName)
            cy.get('[data-i="password"] > input').type(login.Password)
            
        })
        
        cy.get('button').click()
    })
         // This is for Site picker logic with client and site and program// 
         it("Site picker logic", () => {
            
                    cy.get('.intro-location-text').click({force:true})// this is for site picker
                   
                    //  cy.get('.cm-dropdown > .dropdown').should('have.value', '10618').click()
                      

                        cy.get('.cm-dropdown > .dropdown').click().parent().contains('li', 'Apple, Inc.').click()
                                          
                        cy.get(':nth-child(3) > .cm-dropdown > .selection-trigger > .fa').click().parentsUntil().contains('li', 'Elk Grove').click()
                        cy.get(':nth-child(4) > .cm-dropdown > .selection-trigger').click().parentsUntil().contains('li', 'Custodial').click()
                        cy.get('.cm-picker-context > .actions > .primary').click()
                        cy.wait(10000)
        //Click on left Navigation -Charts and details- Audit//
      
     
                                             cy.get('.nav-hamburger').click()
                                                cy.get('.nav-level-title').contains('Charts & Details').click()
                                                    cy.get('.nav-level-title').contains('Quality').click()
                                                        cy.get('.nav-level-button').contains('Audits').click()
            
        //cy.wait(10000)
                       // cy.get('[data-i="link"] > a > div').click() // click on view additional details tab
                      //  cy.get('[title="Charts"] > .tab-button-title').click() //this is for charts tab
                       // cy.get('[title="Details"] > .tab-button-title').click() // this is for details tab
                        
                        cy.get('button').contains('Run Audit').click() // click on create audit button from audit page
                      cy.pause()  
                           
        //Create Audit
         cy.get("select[name='buildingId']").select('1').should('have.value', '10056') //value 1
        
      
        cy.get("select[name='floorId']").select('All').should('have.value', '26011')//option 2 for 200
        //cy.get("select the type of any image")

         
         //cy.get('.Calendar-trigger').click() 
         cy.get('.overall-comment').type('This is for testing')
         cy.get('form > .success').click()
         
              
                              
                  
       
               

                              
       
                
    })
})
       


        
                  
         