//created by: Mayura Vyas Date: 05/07/2019//
//This is a automated script for login to R4 application//
//Select site picker and then Send Survey// 

import { equal } from "assert";

describe('This is automation test suit for R4',  () => {

        it('Visit to insite site',  () => {
       cy.visit('https://redesign-web.sbminsite.com/login')
                                  
    })
        it("Login to application", () => {
        //Login to application with username and pwd//
        cy.fixture("credentials").as("login") //reading cred from file with alias login
        cy.get("@login").then((login) => {
            cy.get('[data-i="username"] > input').type(login.UserName)
            cy.get('[data-i="password"] > input').type(login.Password)
            
        })

        cy.get('button').click()
    }) //this one I just added
       
        
        

        it("Site picker logic", () => {
            cy.get('.intro-location-text').click({force:true})// this is for site picker
            cy.get('.cm-dropdown > .dropdown').click().parent().contains('li', 'Zoetis').click()
                                          
            cy.get(':nth-child(3) > .cm-dropdown > .selection-trigger > .fa').click().parentsUntil().contains('li', 'Charles City').click()
            cy.get(':nth-child(4) > .cm-dropdown > .selection-trigger').click().parentsUntil().contains('li', 'Custodial').click()
            cy.get('.cm-picker-context > .actions > .primary').click()
            cy.wait(20000)
        
                                           
                      
                              //Click on left Navigation -Charts and details- Send Surveys//
           
                                cy.get('.nav-hamburger').click()
                                    cy.get('.nav-level-title').contains('Charts & Details').click()
                                        cy.get('.nav-level-title').contains('Quality').click()
                                                cy.get('.nav-level-button').contains('Surveys').click()
                                                    cy.get('button').contains('Send Survey').click()
                
                                                   // cy.get('.page-action-button-name').contains('Send Survey').click() //Click on send survey button
                                                        cy.get("select[name='programId']").select('Custodial').should('have.value', '1') //value 1 Program
                                                            cy.get(':nth-child(1) > .formik-input').type('mayura vyas') //customer name
                                                                    cy.get(':nth-child(2) > .formik-input').type('mvyas@sbmcorp.com') //email

                                                                        cy.get('.submit').click() //send survey
        })
       

                      
    })