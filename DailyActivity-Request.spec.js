//created by: Mayura Vyas Date: 05/07/2019//
//This is a automated script for login to R4 application//
//Select site picker and then create Audit// 

import { equal } from "assert";

describe('Testing Daily-Requests',  () => {

        it('Visit to insite site',  () => {
        cy.visit('https://bay1.sbminsite.com/login')
        
        
                        
    })
        it('Login to application', () => {
        //Login to application with username and pwd//
        cy.fixture("credentials").as("login") //reading cred from file with alias login
        cy.get("@login").then((login) => {
            cy.get('[data-i="username"] > input').type(login.UserName)
            cy.get('[data-i="password"] > input').type(login.Password)
            
        })
        
        cy.get('button').click()
    })
         // This is for Site picker logic with client and site and program// 
         it("Daily ACtivity", () => {
            
                    
        //Click on left Navigation -Charts and details- Audit//
           
                                             cy.get('.nav-hamburger').click() //left nav
                                                cy.get('.nav-level-title').contains('Daily Activity').click() //Daily activity
                                                    cy.get('.nav-level-title').contains('Requests').click() //Requests
                                                                                                   
                                                        cy.get('button').contains('New Request').click() // Click on New Request

                                                    //Create Request
                                                        cy.get('.employee-selector-wrapper > :nth-child(1) > .form-button').click()// select employee Vickie
                                                        cy.get('.dropdown-trigger').click().parent().contains('li','Time-off').click() //request type
                                                        cy.get('textarea').type('This is for testing') //comment
                                                        cy.get('.submit').click() //submit button

                                                    //This is a second Request
                                                        cy.get('button').contains('New Request').click() // Click on New Request                                               
                                                        cy.get('.employee-selector-wrapper > :nth-child(1) > .form-button').click()// select employee Vickie
                                                        cy.get('.dropdown-trigger').click().parent().contains('li','Question').click() //request type
                                                        cy.get('textarea').type('This is for testing') //comment
                                                        cy.get('.submit').click() //submit button

                                                        cy.wait(5000)

                                                        cy.get(':nth-child(1) > .table-view-link > .medium-body-text').click() //view link
                                                        cy.get('button').contains('ACCEPT').click()

                                                    // This is a third request
                                                        cy.get('button').contains('New Request').click() // Click on New Request   
                                                        cy.get('.employee-selector-wrapper > :nth-child(1) > .form-button').click()// select employee Vickie
                                                        cy.get('.dropdown-trigger').click().parent().contains('li','Meeting with Manager').click() //request type
                                                        cy.get('textarea').type('This is for testing') //comment
                                                        cy.get('.submit').click() //submit button

                                                        

                                                    
                                                    
              
                           
                                                                   
                      
                                  
                                    

                                

                                })
       })



                  
       
               

                              
       
                
 



       


        
                  
         