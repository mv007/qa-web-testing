//created by: Mayura Vyas Date: 05/07/2019//
//This is a automated script for login to R4 application//
//Select site picker and then create Attendance// 

import { equal } from "assert";

describe('Testing Personnel-Attendance',  () => {

        it('Visit to insite site',  () => {
            cy.visit('https://redesign-web.sbminsite.com/login')
               
                        
    })
        it('Login to application', () => {
        //Login to application with username and pwd//
        cy.fixture("credentials").as("login") //reading cred from file with alias login
        cy.get("@login").then((login) => {
            cy.get('[data-i="username"] > input').type(login.UserName)
            cy.get('[data-i="password"] > input').type(login.Password)
            
        })
        
        cy.get('button').click()
    })
         // This is for Site picker logic with client and site and program// 
         it("Site picker logic", () => {
            
                    cy.get('.intro-location-text').click({force:true})// this is for site picker
                      
                        cy.get('.cm-dropdown > .dropdown').click().parent().contains('li', 'Zoetis').click() //client
                                          
                        cy.get(':nth-child(3) > .cm-dropdown > .selection-trigger > .fa').click().parentsUntil().contains('li', 'Lincoln').click()//site
                        cy.get(':nth-child(4) > .cm-dropdown > .selection-trigger').click().parentsUntil().contains('li', 'Custodial').click() //program
                        cy.get('.cm-picker-context > .actions > .primary').click() //save button
                        cy.wait(10000)
        //Click on left Navigation -Charts and details- Conduct//
           
                                             cy.get('.nav-hamburger').click() //left nav
                                                cy.get('.nav-level-title').contains('Charts & Details').click() //chart n details
                                                    cy.get('.nav-level-title').contains('Personnel').click() //personnel
                                                        cy.get('.expand > :nth-child(2) > :nth-child(7) > .nav-level-button > .nav-level-title').click() //Attendance
                                                        
                                                        //go back to this page//
                                                                                                                                           
                            
                                 //Create Attendance entry
                                //first entry      
                                    cy.get('button').contains('Run Attendance').click() // Click on create Attendance
                                    cy.get('.employee-selector-wrapper > :nth-child(2) > .form-button').click()// select employee                     
                                    cy.get('.dropdown-trigger').click().parent().contains('li', 'Arrived Late').click()
                                    cy.get('textarea').type('This is for testing')
                                    cy.get('.submit').click()

                                //Left Early
                                    cy.get('button').contains('Run Attendance').click() // Click on create Attendance
                                    cy.get('.employee-selector-wrapper > :nth-child(2) > .form-button').click()// select employee
                                    cy.get('.dropdown-trigger').click().parent().contains('li', 'Left Early').click()
                                    cy.get('textarea').type('This is a test case')
                                    cy.get('.submit').click()
                                    
                                //Absent
                                    cy.get('button').contains('Run Attendance').click() // Click on create Attendance
                                    cy.get('.employee-selector-wrapper > :nth-child(2) > .form-button').click()// select employee
                                    cy.get('.dropdown-trigger').click().parent().contains('li', 'Absent').click()
                                    cy.get('textarea').type('testing')
                                    cy.get('.submit').click()
                                    cy.get('.submit').click()
                                                                       
                                    
                                    }

                     
                                                            
                                  

                                 

         })
        
        
       })



                  
       
               

                              
       
                
 



       


        
                  
         