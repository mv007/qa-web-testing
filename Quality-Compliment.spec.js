//created by: Mayura Vyas Date: 05/07/2019//
//This is a automated script for login to R4 application//
//Select site picker and then create Compliment// 

import { equal } from "assert";

describe('Testing for Compliment',  () => {

      it('Visit to insite site',  () => {
            cy.visit('https://redesign-web.sbminsite.com/login')
                                  
    })
      it('Login to application', () => {
        //Login to application with username and pwd//
            cy.fixture("credentials").as("login") //reading cred from file with alias login
              cy.get("@login").then((login) => {
                cy.get('[data-i="username"] > input').type(login.UserName)
                  cy.get('[data-i="password"] > input').type(login.Password)
            
        })
       
                      cy.get('button').click()
      })
              
        // This is for Site picker logic with client and site and program//    
        it("Site picker logic", () => {   
                          cy.get('.intro-location-text').click({force:true})// this is for site picker
                          
                     // cy.get('.cm-dropdown > .dropdown').should('have.value', '10618').click()

                              cy.get('.cm-dropdown > .dropdown').click().parent().contains('li', 'Zoetis').click()
                                          
                                cy.get(':nth-child(3) > .cm-dropdown > .selection-trigger > .fa').click().parentsUntil().contains('li', 'Lincoln').click()
                                  cy.get(':nth-child(4) > .cm-dropdown > .selection-trigger').click().parentsUntil().contains('li', 'Custodial').click()
                                         cy.get('.cm-picker-context > .actions > .primary').click()
        // Now Click on view additional details tab for Audits//
                                           cy.wait(20000)
                                              cy.get('.nav-hamburger').click() //click on left hand nav
                                                cy.get('.nav-level-title').contains('Charts & Details').click()
                                                  cy.get('.nav-level-title').contains('Quality').click()
                                                          cy.get('.nav-level-button').contains('Compliments').click()
                                                                cy.get('button').contains('New Compliment').click()
                                              // cy.get('.nav-menu-top > :nth-child(3) > :nth-child(1) > .nav-level-title').click()//click on charts and details
      
                                                     // cy.get('.expand > :nth-child(2) > :nth-child(1) > :nth-child(1)').click({force: true})  //open quality
//cy.get(':nth-child(2) > .expand > :nth-child(2) > :nth-child(3) > .nav-level-button > .nav-level-title').click() //open compliment
        

      
                                                                 // cy.get('.large').click() //create compliment
        
                                cy.get('.dropdown-trigger').click().parent().contains('li','Custodial').click() 
                                  cy.get('.form-button').click()

                                    cy.get('form').within(() => {
                     
                                         cy.get('input[name=CustomerName]').type('Matt') //cust name
            
        cy.get(':nth-child(3) > [style="width: 50%; margin-left: 4px;"] > .formik-group > .dropdown-container > .dropdown-trigger').click().parent().contains('li','Cost').click()
        cy.get('#Description').type('This is a test')
        cy.get('[style="width: 50%; margin-right: 4px;"] > .formik-group > .formik-input').type('(123)-456-7890')
        //cy.get('#multi-select-trigger').click().parent().contains('li','YOLANDA JIMENEZ').click()
        cy.get('[style="width: 50%; margin-right: 4px;"] > .formik-group > .dropdown-container > .dropdown-trigger').click().parent().contains('li','Kingberd').click()
        cy.get(':nth-child(6) > [style="width: 50%; margin-left: 4px;"] > .formik-group > .dropdown-container > .dropdown-trigger').click().parent().contains('li','1').click()
  
           

        })
        
                    cy.get('form').submit()
      

    })
})
