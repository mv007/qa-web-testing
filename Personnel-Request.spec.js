//created by: Mayura Vyas Date: 05/07/2019//
//This is a automated script for login to R4 application//
//Select site picker and then create Audit// 

import { equal } from "assert";

describe('Testing Personnel-Requests',  () => {

        it('Visit to insite site',  () => {
        cy.visit('https://redesign-web.sbminsite.com/login')
        
        
                        
    })
        it('Login to application', () => {
        //Login to application with username and pwd//
        cy.fixture("credentials").as("login") //reading cred from file with alias login
        cy.get("@login").then((login) => {
            cy.get('[data-i="username"] > input').type(login.UserName)
            cy.get('[data-i="password"] > input').type(login.Password)
            
        })
        
        cy.get('button').click()
    })
         // This is for Site picker logic with client and site and program// 
         it("Site picker logic", () => {
            
                    cy.get('.intro-location-text').click({force:true})// this is for site picker
                      
                        cy.get('.cm-dropdown > .dropdown').click().parent().contains('li', 'Zoetis').click() //client
                                          
                        cy.get(':nth-child(3) > .cm-dropdown > .selection-trigger > .fa').click().parentsUntil().contains('li', 'Lincoln').click()//site
                        cy.get(':nth-child(4) > .cm-dropdown > .selection-trigger').click().parentsUntil().contains('li', 'Custodial').click() //program
                        cy.get('.cm-picker-context > .actions > .primary').click() //save button
                        cy.wait(10000)
        //Click on left Navigation -Charts and details- Audit//
           
                                             cy.get('.nav-hamburger').click() //left nav
                                                cy.get('.nav-level-title').contains('Charts & Details').click() //chart n details
                                                    cy.get('.nav-level-title').contains('Personnel').click() //personnel
                                                    cy.get(':nth-child(2) > .expand > :nth-child(2) > :nth-child(1) > .nav-level-button > .nav-level-title').click() //req

                                               
                                                        cy.get('button').contains('New Request').click() // Click on New Request

                                                    //Create Request
                                                        cy.get('.employee-selector-wrapper > :nth-child(3) > .form-button').click()// select employee Carlos
                                                        cy.get('.dropdown-trigger').click().parent().contains('li','Time-off').click() //request type
                                                        cy.get('textarea').type('This is for testing') //comment
                                                        cy.get('.submit').click() //submit button

                                                    //This is a second Request
                                                        cy.get('button').contains('New Request').click() // Click on New Request                                               
                                                        cy.get('.employee-selector-wrapper > :nth-child(3) > .form-button').click()// select employee Carlos
                                                        cy.get('.dropdown-trigger').click().parent().contains('li','Question').click() //request type
                                                        cy.get('textarea').type('This is for testing') //comment
                                                        cy.get('.submit').click() //submit button
                                                    // This is a third request
                                                        cy.get('button').contains('New Request').click() // Click on New Request   
                                                        cy.get('.employee-selector-wrapper > :nth-child(3) > .form-button').click()// select employee Carlos
                                                        cy.get('.dropdown-trigger').click().parent().contains('li','Meeting with Manager').click() //request type
                                                        cy.get('textarea').type('This is for testing') //comment
                                                        cy.get('.submit').click() //submit button

                                                    
                                                    
              
                           
                                                                   
                      
                                  
                                    

                                

                                })
       })



                  
       
               

                              
       
                
 



       


        
                  
         